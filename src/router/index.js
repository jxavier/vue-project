import Vue from 'vue'
import Router from 'vue-router'
import todos from '../pages/TodosPage.vue'
import users from '../pages/UsersPage'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: users
        },
        {
            path: '/users',
            name: 'users',
            component: users
        },
        {
            path: '/todos',
            name: 'todos',
            component: todos
        }
    ]
})
